package com.springbootmicroservicetest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class ServiceController {

	@RequestMapping("/microservice-test")
	public String index() {
		return "Greetings from Spring Boot!\n";
	}

}